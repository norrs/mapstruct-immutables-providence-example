package no.norrs.mapstruct_immutable_providence_example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ExampleMapperTest {

    @Test
    public void testFoo() {
        Bar.Builder barBuilder = new Bar.Builder().uuid("foo");
        assertThat(ExampleMapper.INSTANCE.mapFrom(barBuilder.build()))
                .isEqualTo(new Foo.Builder().id("foo").build());

    }

    @Test
    public void testSerialize() throws JsonProcessingException {
        Bar.Builder barBuilder = new Bar.Builder().uuid("foo");

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(ExampleMapper.INSTANCE.mapFrom(barBuilder.build()));
        assertThat(json).isEqualTo("{\"id\":\"foo\"}");
    }

}