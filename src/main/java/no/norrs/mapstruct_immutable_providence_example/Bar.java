package no.norrs.mapstruct_immutable_providence_example;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize
@MapStructSupport
public interface Bar {
    String getUuid();

    static Builder builder() {
        return new Builder();
    }
    class Builder extends ImmutableBar.Builder {}
}