package no.norrs.mapstruct_immutable_providence_example;


import org.immutables.value.Value;

import static org.immutables.value.Value.Style.ImplementationVisibility.PACKAGE;

@Value.Style(builder = "new", visibility = PACKAGE, overshadowImplementation = true)
public @interface MapStructSupport {}
