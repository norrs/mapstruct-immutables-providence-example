package no.norrs.mapstruct_immutable_providence_example;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = Foo.Builder.class)
@MapStructSupport
public interface Foo {
    String getId();

    static Builder builder() {
        return new Builder();
    }
    class Builder extends ImmutableFoo.Builder {}

}