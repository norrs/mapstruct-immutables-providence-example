package no.norrs.mapstruct_immutable_providence_example;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExampleMapper {

    ExampleMapper INSTANCE = Mappers.getMapper(ExampleMapper.class);

    @Mapping(source= "uuid", target="id")
    Foo mapFrom(Bar bar);
}
